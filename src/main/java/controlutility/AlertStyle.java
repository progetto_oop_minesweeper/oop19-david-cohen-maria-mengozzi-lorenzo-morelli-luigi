package controlutility;

import javafx.scene.control.Alert;

/**
 * Interface to set Alert Style.
 */
public interface AlertStyle {
    /**
     * @param alert to set Style*/
    void setStyle(Alert alert);
}
